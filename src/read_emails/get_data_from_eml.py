from email.contentmanager import raw_data_manager
from email.mime import message
import pathlib
import pandas as pd
from email import header, policy
from email.parser import BytesParser
import email
from PIL import Image
import re

# przetwarzanie pliku w formacie eml
def read_eml(file):
    output_count = 0
    attachments = []
    image = 'no'
    with open(file, 'rb') as f:
        msg = BytesParser(policy=policy.default).parse(f)
        sender = msg['from']
        subject = msg['subject']
        date = msg['date']
        # załączniki
        for attachment in msg.iter_attachments():
            try:
                output_filename = attachment.get_filename()
                attachments.append(output_filename)
                if output_filename.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
                    image =  'yes'
        # nazwa załącznika, który nie jest obrazem (filename not an image file)
            except AttributeError:
                    print("Got string instead of filename for %s. Skipping." % f.name)
                    continue
             # brak załącznika
        
        # treść wiadomości email (body text)
        content = msg.get_body(preferencelist=('plain')).get_content()
        try:
            html = msg.get_body(preferencelist=('html')).get_content()
        except:
            html = msg.get_body(preferencelist=('plain')).get_content()
        
    f.close()
    return content, sender, subject, date, attachments, image, msg, html

# ekstrakcja tekstu linkującego z znacznika a w html
def get_text_link_from_a_tags(html):
    a_tag_regex = r"(\<a href=(.*?)\>(.*?)\<\/a\>)"
    a_tags = re.findall(a_tag_regex, html)
    for a in a_tags:
        if "<img" in a[0]: # pominięcie linków w obrazkach
            a_tags.remove(a)
            
    link_text=[]
    for a in a_tags:
        link_text.append(a[2])
    url_regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    for link in link_text:
        if re.findall(url_regex,link):
            link_text.remove(link) # pominięcie linkujących adresów url 

    return link_text # linki tekstowe

# licznik słów z czarnej listy blacklist.txt
def count_blackwords(text):
    blacklist = open("src/read_emails/blacklist.txt")
    blacklist= blacklist.read()
    i=0
    counter = 0
    text = text.lower()
    text = re.sub("[^a-zA-Z]+", " ", text)
    for word in text.split():
        if(text.split()[i] in blacklist.split()):
            counter = counter +1  
        else:
            counter
        i=i+1
    return(counter)

# licznik słów z czarnej listy dla linków 
def count_link_blackwords(text_link):
    blacklist = open("src/read_emails/link_blacklist.txt")
    blacklist= blacklist.read()
    i=0
    counter = 0
    text_link = text_link.lower()
    text_link = re.sub("[^a-zA-Z]+", " ", text_link)
    for word in text_link.split():
        if(text_link.split()[i] in blacklist.split()):
            counter = counter +1  
        else:
            counter
        i=i+1
    return(counter)

# analiza tekstu 
# zwracana liczba znaków, znaków nie alfanumerycznych, liczb, słów oraz unikatowych słów w tekście
def count_text(text):
    txt= re.sub("[^a-zA-Z]+", " ", text)
    txt_no_spaces = re.sub(r"\s+", "", text)
    no_chars = len(txt_no_spaces)
    alph_chars = re.sub("[^a-zA-Z\s]","",txt_no_spaces)
    no_num = re.sub("[^0-9\s]","",txt_no_spaces)
    no_num = len(no_num)
    no_non_alph_chars = no_chars-len(alph_chars)
    no_words = len(txt.lower().split())
    no_unique_words = len(set(txt.split()))

    return no_chars, no_non_alph_chars, no_num, no_words, no_unique_words

# sprawdzenie obecności adresów rozpoczynających się od http
def check_for_http(list):
    http_regex = r"(http://.*?)"
    http_protocol=[]
    for i in list:
        if re.findall(http_regex,i):
            http = re.findall(http_regex,i)
            http_protocol.append(http)

    return len(http_protocol)

# analiza tytułu maila  
# zwracana liczba słów z blacklisty, czy jest reply word, liczba znaków, liczba znaków nie alfanumerycznych, liczba słów
def analyze_subject(subject):
    blw = count_blackwords(subject)
    no_chars, no_non_alph_chars, no_num, no_words, no_unique_words = count_text(subject)
    subject = subject.lower()

    # check for reply word in subject
    re_word='(re:.*$)'
    i = 0
    reply = 0
    
    if(re.search(re_word, subject)):
        reply = 1
    else:
        reply = 0
    
    return blw, reply, no_chars, no_non_alph_chars, no_words

# analiza godziny otrzymania maila 
def analyze_date(hours):   
    standard_hours = 0
    time_format = re.compile("[\d]{2}:") # format czasu
    time = time_format.findall(hours) 
    hours = re.sub("[^0-9\s]","",time[0])
    # godzina z zakresu [8,18] - czas standardowy (godziny robocze)
    if (int(hours) >= 8 and int(hours) <= 18): 
        standard_hours = 1
    else:
        standard_hours = 0
    return standard_hours

# analiza nadawcy 
# liczba słów w polu nadawcy, długość domeny (ile części), zakończenie domeny, liczby w polu nadawcy  
def analyze_sender(sender): 
    sender = sender.lower()
    sender_mail_regex  = r"(\<(.*?)@(.*?)\>)"
    sender_mail = re.findall(sender_mail_regex,sender)
    sender_name = re.sub("(\<(.*?)@(.*?)\>)","",sender)
    sender_name = re.sub("(\")"," ",sender_name)

    sm = sender_mail[0][0]
    
    domain_ending = 0
    no_chars, no_non_alph_chars, no_num, no_words, no_unique_words = count_text(sender_name)
    sender_no_words = no_words
    sender_no_num = no_num
    sm = sm.split("@")
    domains = sm[1].split(".")
    
    domains_len = len(domains) # SenderNoPartsInDomain - liczba części składowych domeny
    ending = domains[domains_len-1]
    ending = re.sub("[^a-zA-Z\s]","", ending)
    if ending == "com":
        domain_ending = 1
    elif ending == "edu":
        domain_ending = 2
    elif ending == "org":
        domain_ending = 3
    elif ending == "us":
        domain_ending = 4
    elif ending == "gov":
        domain_ending = 5
    elif ending == "ca":
        domain_ending = 6
    elif ending == "au":
        domain_ending = 7
    elif ending == "net":
        domain_ending = 8
    elif ending == "uk":
        domain_ending = 9
    else :
        domain_ending = 10 # inne 
    return sender_no_words, domains_len, domain_ending, sender_no_num

# analiza treści maila 
def analyze_body(body):  
    money_sign = 0 # waluta w treści
    c  = re.sub("(\[data:image(.*?)\])", "", body) # pominięcie obrazka 
    no_chars, no_non_alph_chars, no_num, no_words, no_unique_words = count_text(c) # analiza tekstu
    body_no_chars = no_chars
    body_no_non_alph_chars = no_non_alph_chars
    body_no_num = no_num
    body_no_words = no_words
    body_no_unique_words = no_unique_words
    body_no_blackwords = count_blackwords(c) # liczbnik słów z BL
    for word in c.lower():
        if word == "$" or word == "usd" or word == "euro" or word == "cad":
            money_sign = money_sign + 1 
    # obecność adresu url 
    url_regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    # obecność linka (nie adres - tekst linkujący)
    link_regex = r"(\<[A-Za-z]+://.*?\>)" # np.: Click here<https://www=2Eelka=2Epw=2Eedu=2Epl>
    url = re.findall(url_regex,body)
    final_url=[]
    for u in url:
        final_url.append(u[0])

    links = re.findall(link_regex,body)
    final_links=[] 
    for link in links:
        link=link.replace("<", "")
        link=link.replace(">","")
        final_links.append(link)

    #no_url = len(url)-len(links)

    raw_url= set(final_url) - set(final_links) # raw url in text
    no_url=len(raw_url)
    
    no_http_protocol = check_for_http(raw_url)

    no_http_links = check_for_http(final_links)

    # liczba obrazków, linkujących obrazków
    # img_regex = r"(\<img(.*?)\>)" obraz w html
    img_regex = r"(\[data:image(.*?)\])" # obraz w eml [data:image]
    img_link_regex = r"(\[data:image(.*?)\]\<http(.*?)\>)" # obrazek linkujący w eml [data:image*]<http://*>
    img = re.findall(img_regex,body)
    img_links = re.findall(img_link_regex,body)
    no_img = len(img)
    no_img_links = len(img_links)

    return body_no_chars, body_no_non_alph_chars, body_no_num, body_no_words, body_no_unique_words, body_no_blackwords, money_sign, no_url, no_http_protocol, no_http_links, no_img, no_img_links
# Atrybuty:
# BodyNoChars,NoNonAlphabeticChars,NoNumbersInBody,
# BodyNoWords,BodyNoUniqueWords,NoBlacklistWordsInBody,MoneySign,
# NoURL,HttpProtocol,NoTextLinks,NoBlacklistWordsLinks,LinkToHttp,NoImages,NoImageLinks,

# analiza załącznika
def analyze_attachments(attachments):
    no_attachments = len(attachments)
    wicked_attachment = 0
    # doble extention to do
    for attachment in attachments:
        attachment = attachment.lower()
        ext = pathlib.Path(attachment).suffixes
        # podejrzane załączniki np.: zip, doc, docm, xls, xlsm, rar, html, exe, msi, scr, jar 
        # potencjalnie bezpieczne załaczniki
        safe_extentions = ['.pdf', '.docx', '.xlsx', '.pptx', '.jpg', '.jpeg', '.png', '.mp3', '.mp4', '.txt', '.csv']
        
        if len(ext)==1: # więcej niz jedno rozszerzenie - podejrzane (np.: plik.pdf.exe)
            if filter(lambda x: ext[0] in x, safe_extentions):
                wicked_attachment
            else:
                wicked_attachment = wicked_attachment + 1
        else:
            wicked_attachment = wicked_attachment + 1
    return no_attachments, wicked_attachment

# analiza linków tekstowych (liczba + słowa z BL)
def analyze_links(html):
    text_links = get_text_link_from_a_tags(html)
    blw = []
    for link in text_links:
        blw.append(count_link_blackwords(link))
    no_link_blackwords = sum(blw)

    no_text_links = len(text_links)
    return no_text_links, no_link_blackwords

# ekstrakcja unikatowych rozszerzeń
def get_no_unique_file_extentions(attachments, content):
    extentions = []
    for attachment in attachments:
        attachment = attachment.lower()
        ext = pathlib.Path(attachment).suffixes 
        for e in ext:
            e = re.sub("[^a-zA-Z]+", "", e)
            extentions.append(e)
    
    all_extentions  = open("src/read_emails/file_extentions.txt")
    all_extentions= all_extentions.read()
    i=0
    content = content.lower()
    content = re.sub("(\[data:image(.*?)\])", "", content)
    
    content = re.sub("[^a-zA-Z]+", " ", content)
    for word in content.split():
        if(content.split()[i] in all_extentions.split()):
            extentions.append(content.split()[i])
        else:
            extentions
        i=i+1

    # usunięcie duplikatów 
    extentions = list(dict.fromkeys(extentions))

    return len(extentions)

# WSZYSTKIE ATRYBUTY:
#id,blacklistWordsInTitle,SubjectReplyWord,SubjectNoChars,SubjectNonAlphabeticChars,SubjectNoWords,standardHours,SenderNoWords,SenderNoPartsInDomain,DomainEnding,NoNumbersInSender
# BodyNoChars,NoNonAlphabeticChars,NoNumbersInBody,
# BodyNoWords,BodyNoUniqueWords,NoBlacklistWordsInBody,MoneySign,NoURL,HttpProtocol,
# NoTextLinks,NoBlacklistWordsLinks,LinkToHttp,NoImages,NoImageLinks,
# NoAttachment, NoFileExtention,WickedAttachment,phishing

# Przetwarzanie wiadomości mailowej w celu ekstrakcji atrybutów
def analyze_eml(eml):
    content, sender, subject, date, attachments, image, msg, html = read_eml(eml)
    result = []

    # blacklistWordsInTitle,SubjectReplyWord,SubjectNoChars,SubjectNonAlphabeticChars, SubjectNoWords
    subject_vector = analyze_subject(subject)
    for subj in subject_vector:
        result.append(subj)
    result.append(analyze_date(date)) # standardHours

    # SenderNoWords,SenderNoPartsInDomain,DomainEnding,NoNumbersInSender
    sender_vector = analyze_sender(sender)
    
    for send in sender_vector:
        result.append(send)

    #BodyNoChars,NoNonAlphabeticChars,NoNumbersInBody,BodyNoWords,BodyNoUniqueWords,NoBlacklistWordsInBody,MoneySign,NoURL,HttpProtocol,
    
    body_no_chars, body_no_non_alph_chars, body_no_num, body_no_words, body_no_unique_words, body_no_blackwords, money_sign, no_url, no_http_protocol, no_http_links, no_img, no_img_links = analyze_body(content)
    
    cv1 = [body_no_chars, body_no_non_alph_chars, body_no_num, body_no_words, body_no_unique_words, body_no_blackwords, money_sign, no_url, no_http_protocol]
    cv2 = [no_http_links, no_img, no_img_links]
    for c1 in cv1:
        result.append(c1)

    #NoTextLinks,NoBlacklistWordsLinks,
    for l in analyze_links(html):
        result.append(l)
    
    #LinkToHttp,NoImages,NoImageLinks
    for c2 in cv2:
        result.append(c2)

    no_attachments, wicked_attachment = analyze_attachments(attachments)
    #NoAttachment
    result.append(no_attachments)
    #NoFileExtention 
    result.append(get_no_unique_file_extentions(attachments, content))

    #WickedAttachment
    result.append(wicked_attachment)

    return result

#eml_file = "phishing-detect/draft/emails/new-file.eml"
#res = analyze_eml(eml_file)
#print(res)
#print(len(res))