import argparse
import os
from distutils.spawn import find_executable
import csv
from socket import MsgFlag
import pandas as pd
import re
import pathlib
from IPython.display import display
from read_emails import analyze_eml

from mlmodels import classification_result

def main():
    parser = argparse.ArgumentParser(description="Phishing detection")

    parser.add_argument('file', help='EML file to check if it is phishing')
   
    args = parser.parse_args()

    # wszytskie atrybuty 
    columns = ["blacklistWordsInTitle","SubjectReplyWord","SubjectNoChars","SubjectNonAlphabeticChars","SubjectNoWords","standardHours","SenderNoWords","SenderNoPartsInDomain","DomainEnding","NoNumbersInSender","BodyNoChars","NoNonAlphabeticChars","NoNumbersInBody","BodyNoWords","BodyNoUniqueWords","NoBlacklistWordsInBody","MoneySign","NoURL","HttpProtocol","NoTextLinks","NoBlacklistWordsLinks","LinkToHttp","NoImages","NoImageLinks","NoAttachment","NoFileExtention","WickedAttachment"]
    
    # plik z wiadomością
    file_path = args.file
    # błąd z wgraniem pliku
    if not os.path.isfile(file_path):
        print(f'\033[31;1m[-] File {file_path} does not exist!\033[0m')
        exit()
    
    else:
        result = analyze_eml(file_path) # przetwarzanie wiadomości
        result_file = 'src/temp/result.csv' 
        # zapisanie wartości atrybutów do pliku (wektor testowy)
        with open(result_file, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(columns)
            writer.writerow(result)
        print("Vector mail:" , result)
    
    # wektor testowy
    df_test = pd.read_csv(result_file)

    # wyniki klasyfikacji 
    res = classification_result(df_test)
    print("Classification results")
    print(res)
    # głosowanie większościowe 
    if res['Result'].sum()>=5:
        print("*** Wysokie ryzyko phishingu ***")
    elif res['Result'].sum()>=3:
        print("** Średnie ryzyko phishingu **")
    else:
        print("* Niskie ryzyko phishingu *")

import sys
from PyQt5 import uic
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QDialog, QApplication, QLabel, QWidget, QPushButton, QInputDialog, QLineEdit, QFileDialog, QMessageBox, QTableWidget, QTableWidgetItem, QVBoxLayout
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt

from csv import DictReader

import sqlite3
# wyniki klasyfikacji 
def return_result(file_path):
        # wszystkie atrybuty
        columns = ["blacklistWordsInTitle","SubjectReplyWord","SubjectNoChars","SubjectNonAlphabeticChars","SubjectNoWords","standardHours","SenderNoWords","SenderNoPartsInDomain","DomainEnding","NoNumbersInSender","BodyNoChars","NoNonAlphabeticChars","NoNumbersInBody","BodyNoWords","BodyNoUniqueWords","NoBlacklistWordsInBody","MoneySign","NoURL","HttpProtocol","NoTextLinks","NoBlacklistWordsLinks","LinkToHttp","NoImages","NoImageLinks","NoAttachment","NoFileExtention","WickedAttachment"]
        result = analyze_eml(file_path) # przetwarzanie pliku eml w celu ekstrakcji atrybuów
        result_file = 'src/temp/result.csv'
        with open(result_file, 'w') as file: 
            writer = csv.writer(file)
            writer.writerow(columns)
            writer.writerow(result) # zapis wartości atrybutów do pliku -> wektor testowy
    
        df_test = pd.read_csv(result_file) # wektor testowy

        res = classification_result(df_test) # klasyfikacja dla przypadku testowego 
        return(res)

# Pierwszy widok
class HomeScreen(QDialog):
    def __init__(self):
        super(HomeScreen, self).__init__()
        loadUi("src/home.ui",self)

        self.button = self.findChild(QPushButton, "fileChoose")

        self.label = self.findChild(QLabel, "plik")

        self.button.clicked.connect(self.gotofiledialog)

    # otwórz okno z wyborem pliku
    def gotofiledialog(self):
        fname = QFileDialog.getOpenFileName(self, "Open File", "", "All Files (*);; EML Files (*.eml)")
        
        if fname:
            path = pathlib.PurePath(fname[0])
            self.label.setText("Wybrany plik: " + path.name)
            self.gonext()
            # zapis ściezki do pliku
            path_file = 'src/temp/path.txt'
            with open(path_file, 'w') as f:
                f.write(str(path))
    
    @pyqtSlot()        
    def gonext(self):
        button2 = QPushButton('Przejdź dalej', self)
        button2.setGeometry(250, 340, 161, 51)
        button2.setStyleSheet("border-radius:20px; background-color: rgb(44, 188, 243);")
        
        button2.show()         
        button2.clicked.connect(self.gotoresult)

    def gotoresult(self):
        res = ResultScreen()
        widget.addWidget(res)
        widget.setCurrentIndex(widget.currentIndex()+1)

class QLabelinfo(QLabel):
    clicked=pyqtSignal()

    def mousePressEvent(self, ev):
        self.clicked.emit()

# Widok drugi - decyzja finalna
class ResultScreen(QDialog):
    def __init__(self):
        super(ResultScreen, self).__init__()
        loadUi("src/result.ui",self)
        self.button = self.findChild(QPushButton, "backToHome")

        self.button.clicked.connect(self.gotohome)

        self.button4 = self.findChild(QPushButton, "infobutton")

        self.button4.setIcon(QtGui.QIcon('src/img/info.png'))
        self.button4.setIconSize(QtCore.QSize(24,24))

        self.button4.clicked.connect(self.showinfo)

        with open('src/temp/path.txt') as f:
            pth = f.read()
            fn = os.path.basename(pth)
            #print(fn)
            self.label = self.findChild(QLabel, "chosen_file") 
            self.label.setText("Wybrany plik: " + fn)

            res = return_result(pth)

            with open('src/temp/result-details.txt', 'w') as file:
                file.write(res.to_string())
                res.to_csv('src/temp/result-details.csv', index=False)


            self.label2 = self.findChild(QLabel, "result") 
            self.label3 = self.findChild(QLabel, "result_ico")
            

            if res['Result'].sum()>=5:
                self.label2.setText("Wysokie ryzyko phishingu")
                self.label3.setStyleSheet("border-image: url(src/img/high.png) 0 0 0 0 stretch stretch;")
            elif res['Result'].sum()>=3:
                self.label2.setText("Średnie ryzyko phishingu")
                self.label3.setStyleSheet("border-image: url(src/img/medium.png) 0 0 0 0 stretch stretch;")
            else:
                self.label2.setText("Niskie ryzyko phishingu")
                self.label3.setStyleSheet("border-image: url(src/img/low.png) 0 0 0 0 stretch stretch;")
            #print(return_result(pth))
            self.button3 = self.findChild(QPushButton, "details")
            self.button3.clicked.connect(self.show_details)

    def show_details(self):
        details = DetailsScreen()
        widget.addWidget(details)
        widget.setCurrentIndex(widget.currentIndex()+1)

    def gotohome(self):
        home = HomeScreen()
        widget.addWidget(home)
        widget.setCurrentIndex(widget.currentIndex()+1)


    def showinfo(self):
        msg = QMessageBox()
        msg.setStyleSheet("font-family: Helvetica Neue; font-size:12px; font-weight: normal;")
        msg.setWindowTitle("Informacja")
        with open('src/img/info.txt') as file:
            info = file.read()
            msg.setText(info)

        msg.setStandardButtons(QMessageBox.Cancel)
        retval = msg.exec_()

# Widok trzeci - szczegółowe wyniki
class DetailsScreen(QDialog):
    def __init__(self):
        super(DetailsScreen, self).__init__()
        loadUi("src/details.ui",self)

        with open('src/temp/result-details.csv', 'r') as f:
            df = pd.read_csv(f)
            i = 0
            while i<7:
                self.tableWidget.setItem((i),0, QTableWidgetItem(df["ML method"][i]))
                if df["Result"][i]=="[1]":
                    self.tableWidget.setItem((i),1, QTableWidgetItem("Tak"))
                else:
                    self.tableWidget.setItem((i),1, QTableWidgetItem("Nie"))
                i=i+1
            
            self.button = self.findChild(QPushButton, "back")

            self.button.clicked.connect(self.gotoresult)

    def gotoresult(self):
        res = ResultScreen()
        widget.addWidget(res)
        widget.setCurrentIndex(widget.currentIndex()+1)
            



if __name__ == '__main__':
    # main
    app = QApplication(sys.argv)
    home = HomeScreen()
    widget = QtWidgets.QStackedWidget()
    widget.addWidget(home)
    widget.setFixedHeight(500)
    widget.setFixedWidth(650)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")        
    #main()
