from .naive_bayes import classifyNB
from .svm import classifySVM
from .knn import classifyKNN
from .decision_tree import classifyDT
from .random_forest import classifyRF
from .mlp import classifyMLP
from .logreg import classifyLR
from .class_result import classification_result
