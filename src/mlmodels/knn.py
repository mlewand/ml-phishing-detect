from sklearn.neighbors import KNeighborsClassifier
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator k-najbliszych sąsiadów
def classifyKNN(test): 
    knn = KNeighborsClassifier(n_neighbors=5)
    
    # trenowanie klasyfikatora na zbiorze trenującym
    knn.fit(X_train, y_train)
 
    # przewidywanie klasy dla przypadku testowego 
    y_pred = knn.predict(test)
    
    return y_pred