import pandas as pd
from sklearn.model_selection import train_test_split

# ściezka do wczytywanego pliku z danymi 
df = pd.read_csv('src/data/raw-data3.csv')
# zmienna do predykcji
predict = "phishing"
data=df.drop(labels='id', axis=1)

X_train = data.drop(predict, axis=1) # zawiera wszystkie atrybuty
y_train = data[predict] # zawiera oczekiwaną decyzję 