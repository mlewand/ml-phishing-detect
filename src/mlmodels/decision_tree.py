from sklearn.tree import DecisionTreeClassifier
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator Decision Tree (Drzewa Decyzyjne)
def classifyDT(test): 
    dt_clf = DecisionTreeClassifier()

    # trenowanie klasyfikatora na zbiorze trenującym
    dt_clf = dt_clf.fit(X_train,y_train)

    # przewidywanie klasy dla przypadku testowego 
    y_pred = dt_clf.predict(test)
    return y_pred