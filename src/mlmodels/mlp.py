from sklearn.neural_network import MLPClassifier
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator MLP Perceptron Wielowarstwowy
def classifyMLP(test): 
    clfMLP = MLPClassifier(hidden_layer_sizes=(150,100,50), max_iter=500,activation = 'relu',solver='adam',random_state=1)

    # trenowanie klasyfikatora na zbiorze trenującym
    clfMLP = clfMLP.fit(X_train,y_train)

    # przewidywanie klasy dla przypadku testowego 
    pred_MLP = clfMLP.predict(test)

    return pred_MLP