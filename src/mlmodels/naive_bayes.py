import numpy as np
import pandas as pd
from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB

#from training_dataset import data
#from training_dataset import predict
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator Naiwnego Bayesa
def classifyNB(test):
    # skalowanie atrybutów (preprocessing)
    sc = StandardScaler()
    X_train1 = sc.fit_transform(X_train)
    X_test = sc.transform(test)
    classifierNB = GaussianNB()

    # trenowanie klasyfikatora na zbiorze trenującym
    classifierNB.fit(X_train1, y_train)

    # przewidywanie klasy dla przypadku testowego 
    y_pred = classifierNB.predict(X_test)

    return y_pred
