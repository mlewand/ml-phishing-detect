from sklearn import svm
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator SVM
def classifySVM(test): 
    svm_clf = svm.SVC(kernel='linear') # Linear Kernel

    # trenowanie klasyfikatora na zbiorze trenującym
    svm_clf.fit(X_train, y_train)

    # przewidywanie klasy dla przypadku testowego 
    y_pred = svm_clf.predict(test)

    return y_pred