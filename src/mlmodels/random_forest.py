from sklearn.ensemble import RandomForestClassifier
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator Random Forest (Losowe Drzewa Decyzyjne)
def classifyRF(test):
    rf_clf=RandomForestClassifier(n_estimators=50)

    # trenowanie klasyfikatora na zbiorze trenującym
    rf_clf.fit(X_train,y_train)

    # przewidywanie klasy dla przypadku testowego 
    y_pred = rf_clf.predict(test)

    return y_pred