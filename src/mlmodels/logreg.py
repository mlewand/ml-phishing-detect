from sklearn.linear_model import LogisticRegression
from .training_dataset import X_train
from .training_dataset import y_train

# Klasyfikator - Regresja Logistyczna 
def classifyLR(test): 
    logreg = LogisticRegression()

    # trenowanie klasyfikatora na zbiorze trenującym
    logreg = logreg.fit(X_train, y_train)

    # przewidywanie klasy dla przypadku testowego 
    predLR = logreg.predict(test)

    return predLR