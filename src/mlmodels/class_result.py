import pandas as pd
from .naive_bayes import classifyNB
from .svm import classifySVM
from .knn import classifyKNN
from .decision_tree import classifyDT
from .random_forest import classifyRF
from .mlp import classifyMLP
from .logreg import classifyLR

# wyniki poszczegolnych predykcji
def classification_result(test_sample):
    nb_predict = classifyNB(test_sample)
    svm_predict = classifySVM(test_sample)
    knn_predict = classifyKNN(test_sample)
    dt_predict = classifyDT(test_sample)
    rf_predict = classifyRF(test_sample)
    mlp_predict = classifyRF(test_sample)
    logreg_predict = classifyRF(test_sample)
    # decyzja zbiorcza
    result = nb_predict + svm_predict + knn_predict + dt_predict + rf_predict + mlp_predict + logreg_predict
    # result max - 7: 100%
    results = [nb_predict, logreg_predict, knn_predict, svm_predict, dt_predict, rf_predict, mlp_predict]
    results_names = ['Naive Bayes','Logistic Regression','K-nearest Neighbours','SVM','Decision Tree','Random Forest','MLP']
    result_df = pd.DataFrame(
    {'ML method': results_names,
     'Result': results
    })
    return result_df

