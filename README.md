# Narzędzie do detekcji phishingu z wiadomości mailowych z wykorzystaniem algorytmów uczenia maszynowego 
Projekt realizowany w ramach pracy inżynierskiej

Monika Lewandowska 

## Wymagania 

* Python 3 (miniconda3)
* Interpreter python 
* Biblioteki:
    * [pandas](https://pypi.org/project/pandas/)
    * [Pillow](https://pypi.org/project/Pillow/)
    * [scikit-learn](https://pypi.org/project/scikit-learn/)
    * [matplotlib](https://pypi.org/project/matplotlib/)
    * [PyQt5](https://pypi.org/project/PyQt5/)

W celu uruchomienia programu należy zainstalować powyższe biblioteki. Można do tego wykorzystać narzędzie pip:
```
pip install [pakiet]
```

## Instalacja i uruchomienie projektu 

1. Sklonuj repozytorium

```
git clone https://gitlab.com/mlewand/ml-phishing-detect.git
```

2. Upewnij się, że masz odpowiednio skonfigurowane środowisko (patrz wymagania powyżej)

3. Uruchom program 

```
cd ml-phishing-detect
python src/main.py
```
Po uruchomieniu powinno otworzyć się okno z aplikacją. 

## Korzystanie z programu 

1. Wybierz plik do analizy klikając w przycisk "Wybierz plik". Otworzy się okno dialogowe, skąd należy wskazać konkretny plik w formacie eml.
Można wybrać przykładowy plik testowy z folderu tests.


    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_10.42.23.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>
    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_10.47.00.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>


2. Po wybraniu pliku jego nazwa pokaże się na ekranie głównym aplikacji. Aby przejść do analizy pliku należy kliknąć przycisk "Przejdź dalej".

    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_10.47.31.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>

3. Program przetworzy odpowiednio plik z wiadomością do postaci wektorowej. Zostanie on poddany analizie z wykorzystaniem wybranych metod uczenia maszynowego. Po zakończonej pracy zostanie zwrócony wynik finalny świadczący o tym czy program wykrył ryzyko phishingu czy też nie. 

    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_11.08.43.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>

4. W celu zobaczenia poszczególnych decyzji algorytmów można kliknąć w przycisk "Pokaż szczegóły". 

    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_11.08.55.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>

5. Po kliknięciu w ikonę informacja pokaże się komunikat ostrzegający przed klikaniem w potencjalnie niebezpieczne linki czy otwieraniem załączników. 

    <img src="./readme-img/Zrzut_ekranu_2023-01-28_o_11.09.11.png" alt="Pierwszy widok po uruchomieniu programu" width="250"/>

6. Aby przeanalizować kolejne wiadomości wystarczy wybrać przycisk "Powrót" i powtórzyć powyższe kroki. Aby zakończyć prace z programem wystarczy zamknąć okno aplikacji. 

